import 'package:dynamic_themes/dynamic_themes.dart';
import 'package:flutter/material.dart';
import 'package:openlibro/presentation/home/home_view_page.dart';
import 'package:openlibro/routes/routes.dart';
void main() => runApp(MyApp());

class AppThemes {
  static const int DarkRed = 0;
  static const int LightBlue = 1;
  static String toStr(int themeId) {
    switch (themeId) {
      case DarkRed:
        return "Dark Red";
      case LightBlue :
        return "Light Blue";
      default:
        return "Unknown";
    }
  }
}
class MyApp extends StatelessWidget {
  static GlobalKey mtAppKey = GlobalKey();
  @override
  Widget build(BuildContext context) {
    final themeCollection = ThemeCollection(
        themes: {
          AppThemes.LightBlue: ThemeData(primarySwatch: Colors.blue),
          AppThemes.DarkRed: ThemeData.from(colorScheme: ColorScheme.dark(primary: Colors.amberAccent, secondary: Colors.amberAccent,)),
        }
    );

    return DynamicTheme(
        themeCollection: themeCollection,
        defaultThemeId: AppThemes.DarkRed, // optional, default id is 0
        builder: (context, theme) =>MaterialApp(
          key: MyApp.mtAppKey,
        debugShowCheckedModeBanner: false,
        title: 'Biblioteca',
          initialRoute: "Home",
        theme: theme, routes: Routes.getRoutes(context:context),
        home:  HomeApp(),
      ),
    );
  }
}
