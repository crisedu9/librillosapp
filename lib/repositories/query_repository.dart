import 'package:dio/dio.dart';
import 'package:logger/logger.dart';
import 'package:openlibro/models/query.dart';
class QueryRepository {
  final String url = "http://openlibrary.org/search.json?";
   Dio dio = Dio();
  Future<List<Doc>> searchBook(String title,searchBloc) async {
    print(title);
    List<Doc> bookList = [];
   Map<String,dynamic> book;
    searchBloc.loadingBook.sink(true);
    try {
      Response res = await dio.get(url, queryParameters: {"q": title,"mode":"ebooks","has_fulltext":true});
      for(book in res.data["docs"]) {
        bookList.add(Doc.fromJson(book));
      }
     searchBloc.listBook.sink(bookList);
      searchBloc.loadingBook.sink(false);
      Logger().log(Level.info,"Buscando libritos :D ${searchBloc.listBook.value}");
    return Future.value(bookList);
    } catch (e) {
      Logger().log(Level.info,"Fallo al buscar libritos :D ${e}");
      return throw Exception('Failed to load Book $e');

    }
  }
}
