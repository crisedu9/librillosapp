import 'package:flutter/material.dart';
import 'package:openlibro/presentation/home/widgets/app_bar.widget.dart';
import 'package:openlibro/presentation/home/widgets/bottom_bar.widget.dart';
import 'package:openlibro/presentation/listBook/list_product_component.dart';
import 'package:openlibro/utilsBloc/bloc_provider.dart';
import 'home_view_bloc.dart';

class HomeApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final HomeViewBloc blocHome = BlocProvider.ofBloc<HomeViewBloc>(context)!;
    return SafeArea(
      child: Scaffold(
        appBar: AppbarWidget(blocHome),
        bottomNavigationBar: BottomNav(blocHome, actIndex: 0),
        body: CreateBody(blocHome,blocHome.searchBloc),
      ),
    );
  }
}

class CreateBody extends StatelessWidget {
  CreateBody(this.blocHome, searchBloc);
  final HomeViewBloc blocHome;


  @override
  Widget build(BuildContext context) {
    List<Widget> widgetsNavigationBottom = <Widget>[
      ListBookComponent(searchBloc:blocHome.searchBloc,valid: true,),
      ListBookComponent(searchBloc:blocHome.searchBloc,valid: false),
    ];
    // TODO: implement build
    return StreamBuilder(
        stream: blocHome.currentIndexBottomNavigation.stream,
        builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
          return Container(
            decoration: BoxDecoration(color: Colors.black),
            child: IndexedStack(
              index: snapshot.hasData ? snapshot.data : 0,
              children: widgetsNavigationBottom,
            ),
          );
        });
  }
}


