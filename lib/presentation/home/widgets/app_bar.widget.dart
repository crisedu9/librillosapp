
import 'package:flutter/material.dart';
import 'package:openlibro/presentation/home/widgets/search_generic.widget.dart';
import '../home_view_bloc.dart';

class AppbarWidget extends StatelessWidget implements PreferredSizeWidget {
  Size get preferredSize => const Size.fromHeight(60);

  const AppbarWidget(this.homeBlock);
  final HomeViewBloc homeBlock;
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    // TODO: implement build

    return PreferredSize(
      preferredSize: preferredSize,
      child: StreamBuilder(
        stream: homeBlock.currentIndexBottomNavigation.stream,
        builder: (context, snapshot) {
          if(snapshot.hasData && snapshot.data==1)return Container(width: 0,height: 0,padding: EdgeInsets.all(0),);
          return AppBar(
            backgroundColor: Colors.transparent,
            actions: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Container(
                    width: 300,
                    margin: EdgeInsets.only(right: 20, left: 20),
                    // padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                    decoration: BoxDecoration(
                        color: Colors.white24,
                        borderRadius: BorderRadius.circular(40),
                        boxShadow: [
                          BoxShadow(
                              offset: Offset(0, 10),
                              color: Color.fromRGBO(128, 128, 128, 0.2),
                              blurRadius: 10)
                        ]),
                    child: SearchGeneric(homeBlock),
                  ),
                ],
              )
            ],
          );
        }
      ),
    );
  }
}