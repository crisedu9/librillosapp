import 'package:flutter/material.dart';
import 'package:openlibro/presentation/home/home_view_bloc.dart';


class StringFieldValidator {
  static String? validate(String value) {
    return value.isEmpty ? 'String can\'t be empty' : null;
  }
}

class SearchGeneric extends StatelessWidget {
   SearchGeneric(this.homeBlock) ;
  HomeViewBloc homeBlock;
  Widget build(BuildContext context) {
    return TextFormField(
      key: Key('Busqueda'),
      style: TextStyle(color: Colors.white),
      onTap: () {},
      onChanged: (title) {
        //   searchBloc.searchBook(title);
      },
      onFieldSubmitted: (title) {
        homeBlock.searchBloc.searchBook(title);
      },
      validator:(title)=>StringFieldValidator.validate(title!) ,
      decoration: InputDecoration(
        contentPadding:
        EdgeInsets.symmetric(horizontal: 40, vertical: 15),
        border: InputBorder.none,
        enabledBorder: InputBorder.none,
        focusedBorder: InputBorder.none,
        suffixIcon: Icon(Icons.search,color: Colors.white,),
        hintStyle: TextStyle(color: Colors.grey[400]),
        hintText: 'Busqueda...',
      ),
    );
  }
}
