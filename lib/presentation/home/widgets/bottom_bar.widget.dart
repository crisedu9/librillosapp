import 'package:flutter/material.dart';
import '../home_view_bloc.dart';

class BottomNav extends StatelessWidget {
  BottomNav(this.blocHome, {int? actIndex});

  final HomeViewBloc blocHome;
  int i = 0;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return StreamBuilder<int>(
        stream: blocHome.currentIndexBottomNavigation.stream,
        builder: (context, snapshot) {
          return Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),

              boxShadow: [
                BoxShadow(color: Colors.grey, spreadRadius: 3),
              ],
            ),
            child: BottomNavigationBar(
              iconSize: 20,
              backgroundColor: Colors.white,
              type: BottomNavigationBarType.shifting,
              selectedIconTheme:
                  IconThemeData(color: Colors.amberAccent, size: 25),
              selectedItemColor: Colors.white,
              selectedLabelStyle: TextStyle(fontWeight: FontWeight.bold),
              unselectedIconTheme: IconThemeData(
                color: Colors.grey,
              ),
              unselectedItemColor: Colors.grey,
              elevation: 0,
              items: [
                BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
                BottomNavigationBarItem(
                    icon: Icon(Icons.star), label: 'Favoritos'),
              ],
              //optional, default as 0
              onTap: (i) {
                blocHome.currentIndexBottomNavigation.sink(i);
              },
              currentIndex: snapshot.data ?? 0,
            ),
          );
        });
  }
}
