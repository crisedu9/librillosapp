import 'package:openlibro/presentation/listBook/search_bloc.dart';
import 'package:openlibro/utilsBloc/bloc_field.dart';
import 'package:openlibro/utilsBloc/bloc_provider.dart';

class HomeViewBloc extends BlocBase {
  //dispose will be called automatically by closing its streams
  static late HomeViewBloc _instance;
  final SearchBloc searchBloc=SearchBloc();
  factory HomeViewBloc() {
    _instance = new HomeViewBloc._internal();
    return _instance;
  }

  final currentIndexBottomNavigation = FieldBlocGeneric<int>(defaultValue: 0);
  HomeViewBloc._internal();
  @override
  void dispose() {
    currentIndexBottomNavigation.dispose();
  }
}
