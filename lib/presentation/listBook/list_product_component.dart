import 'package:flutter/material.dart';
import 'package:openlibro/models/query.dart';
import 'package:openlibro/presentation/listBook/search_bloc.dart';
import 'package:openlibro/presentation/listBook/widgets/card_empty.widget..dart';
import '../../main.dart';
import 'widgets/animation_transition.dart';

class ListBookComponent extends StatelessWidget {
  const ListBookComponent({required this.searchBloc, required this.valid});

  final SearchBloc searchBloc;

  final bool valid;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: searchBloc.loadingBook.stream,
        builder: (BuildContext context, AsyncSnapshot<bool> snap) =>  snap.hasData && snap.data!? Center(child: CircularProgressIndicator()):
      StreamBuilder(
        stream:
            valid ? searchBloc.listBook.stream : searchBloc.Booksavedlist.stream,
        builder: (BuildContext context, AsyncSnapshot<List<Doc>> snapshot) {
          if (snapshot.hasData && snapshot.data!.length > 0) {

            final productos = snapshot.data;
            return ListView.builder(
              addRepaintBoundaries: false,
              addAutomaticKeepAlives: false,
              padding: EdgeInsets.only(bottom: 30, top: 20),
              itemCount: productos!.length,
              itemBuilder: (context, i) => Hero(
                  transitionOnUserGestures: true,
                  tag: "imageHero${productos[i].coverI}",
                  child: _createItem(context, productos[i], searchBloc)),
            );
          } else {
            return CardEmpty(valid);
          }
        },
      ),
    );
  }
}

Widget _createItem(BuildContext context, Doc producto, SearchBloc searchBloc) {
  return Card(
    color: Colors.white38,
    shadowColor: Colors.amberAccent,
    elevation: 20,
    child: GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          CustomNamedPageTransition(MyApp.mtAppKey, 'DetailBook',
              arguments: producto),
        );
      },
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.amberAccent,
          ),
          borderRadius: BorderRadius.circular(5.0),
        ),
        padding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
        width: 150,
        height: 150,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              flex: 4,
              child: Container(
                child: FadeInImage.assetNetwork(
                    placeholder: 'assets/book.png',
                    image: "http://covers.openlibrary.org/b/id/" +
                        producto.coverI.toString() +
                        "-L.jpg?default=false",
                    width: 190),
              ),
            ),
            Flexible(
              flex: 8,
              child: Container(
                padding: EdgeInsets.all(5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.only(bottom: 10),
                      child: Text(
                        producto.title!.length > 50
                            ? producto.title!.substring(0, 47) + "..."
                            : producto.title!,
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    Container(
                      child: Text(
                          producto.authorName != null
                              ? producto.authorName![0]
                              : "",
                          style: TextStyle(color: Colors.white)),
                    ),
                    Container(
                      child: Text(
                          producto.firstPublishYear != null
                              ? producto.firstPublishYear.toString()
                              : '',
                          style: TextStyle(color: Colors.white)),
                    ),
                  ],
                ),
              ),
            ),
            Flexible(
              flex: 4,
              child: Container(
                child: StreamBuilder<bool>(
                    stream: producto.favorite!.stream,
                    builder: (context, snapshot) {
                      return ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            shape: CircleBorder(),
                            padding: EdgeInsets.all(20),
                            primary: Colors.amberAccent,
                            minimumSize: Size(24, 26)),
                        child: Icon(
                          snapshot.hasData && snapshot.data!
                              ? Icons.star
                              : Icons.star_border,
                          color: snapshot.hasData && snapshot.data!
                              ? Colors.white
                              : Colors.white,
                        ),
                        onPressed: () {
                          List<Doc> bookTemp = searchBloc.Booksavedlist.value!;
                          if (snapshot.hasData && snapshot.data!) {
                            producto.favorite!.sink(false);
                            bookTemp.remove(producto);
                            searchBloc.Booksavedlist.sink(bookTemp);
                          } else {
                            producto.favorite!.sink(true);
                            bookTemp.add(producto);
                            searchBloc.Booksavedlist.sink(bookTemp);
                          }
                        },
                      );
                    }),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}
