import 'package:flutter/material.dart';
import 'package:flutter_lorem/flutter_lorem.dart';
import 'package:openlibro/models/query.dart';
import 'package:openlibro/presentation/listBook/search_bloc.dart';
import 'package:openlibro/utilsBloc/bloc_provider.dart';

class BookDetail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final SearchBloc bloc = BlocProvider.ofBloc<SearchBloc>(context)!;
    final args = ModalRoute.of(context)!.settings.arguments as Doc;
    bloc.onViewInit(args);
    bool _loadImageError = false;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amberAccent,
      ),
      body: ListView(padding: EdgeInsets.only(bottom: 20, top: 50), children: [
        Center(
          /** Card Widget **/
          child: Hero(
            transitionOnUserGestures: true,
            tag: "imageHero${args.coverI}",
            child: Card(
              elevation: 20,
              shadowColor: Colors.amberAccent,
              color: Colors.white38,
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.amberAccent,
                  ),
                  borderRadius: BorderRadius.circular(5.0),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(60.0),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        CircleAvatar(
                          backgroundColor: Colors.amberAccent,
                          radius: 104,
                          child: CircleAvatar(
                            child: _loadImageError
                                ? Text("Error loading image!")
                                : null,
                            foregroundColor: Colors.white,
                            backgroundColor: Colors.amberAccent,
                            backgroundImage: NetworkImage(
                                "http://covers.openlibrary.org/b/id/" +
                                    args.coverI.toString() +
                                    "-L.jpg?default=false"),
                            onBackgroundImageError: _loadImageError
                                ? null
                                : (exception, stackTrace) {
                                    print("Error loading image! " +
                                        exception.toString());
                                    _loadImageError = true;
                                  },
                            radius: 200,
                          ), //CircleAvatar
                        ), //CirclAvatar
                        SizedBox(
                          height: 10,
                        ), //SizedBox
                        Container(
                          padding: EdgeInsets.all(5),
                          child: Text(
                            args.title!.length > 50
                                ? args.title!.substring(0, 47) + "..."
                                : args.title!,
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.white,
                              fontWeight: FontWeight.w500,
                            ), //Textstyle
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.all(5),
                          child: Text(
                            "${args.authorName!=null? args.authorName![0]:''} - ${args.firstPublishYear != null ? args.firstPublishYear!.toString() : ''}",
                            style: TextStyle(
                              fontSize: 15,
                              color: Colors.white,
                            ), //Textstyle
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.all(5),
                          child: Text(
                            lorem(paragraphs: 1, words: 20),
                            style: TextStyle(
                              fontSize: 15,
                              color: Colors.white,
                            ), //Textstyle
                          ),
                        ),
                      ],
                    ),
                  ), //Column
                ),
              ), //SizedBox
            ),
          ), //Card
        ), //Center
      ]),
    );
  }
}
