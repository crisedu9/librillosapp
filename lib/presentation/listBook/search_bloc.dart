import 'package:openlibro/models/query.dart';
import 'package:openlibro/repositories/query_repository.dart';
import 'package:openlibro/utilsBloc/bloc_field.dart';
import 'package:openlibro/utilsBloc/bloc_provider.dart';
class SearchBloc extends BlocBase {
  final QueryRepository repository =QueryRepository();

  final listBook = FieldBlocGeneric<List<Doc>>(defaultValue: <Doc>[]);

  static SearchBloc? _instance;
  factory SearchBloc() {
    if(_instance==null)_instance = new SearchBloc._internal();
    return _instance!;
  }
  final Booksavedlist= FieldBlocGeneric<List<Doc>>(defaultValue: <Doc>[]);
  final autor =FieldBlocGeneric<String>();
  final name =FieldBlocGeneric<String>();
  final Image =FieldBlocGeneric<int>();
  final firstLoad=FieldBlocGeneric<bool>(defaultValue: false);
  final loadingBook=FieldBlocGeneric<bool>(defaultValue: false);
  onViewInit(Doc args) {
    if(!firstLoad.value!){
      autor.sink(args.authorName!=null?args.authorName![0]:'');
      Image.sink(args.coverI!=null?args.coverI:7149385);
      autor.sink(args.title);
    }
  }
  searchBook(title){
    repository.searchBook(title, this);
  }
  SearchBloc._internal();
  @override
  void dispose() {

  }
}

