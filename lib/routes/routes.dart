import 'package:flutter/cupertino.dart';
import 'package:openlibro/presentation/home/home_view_bloc.dart';
import 'package:openlibro/presentation/home/home_view_page.dart';
import 'package:openlibro/presentation/listBook/detail_book_page.dart';
import 'package:openlibro/presentation/listBook/search_bloc.dart';
import 'package:openlibro/utilsBloc/bloc_provider.dart';
class Routes{
  static getRoutes({BuildContext? context}){
    return{
      'Home': (BuildContext context) =>BlocProvider<HomeViewBloc>(child: HomeApp(),bloc: HomeViewBloc(),),
      'DetailBook': (BuildContext context) =>BlocProvider<SearchBloc>(child: BookDetail(),bloc: SearchBloc(),),
    };
  }
}
