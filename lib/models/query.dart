
import 'dart:convert';

import 'package:openlibro/utilsBloc/bloc_generic.dart';

QueryResponseModel queryResponseModelFromJson(String str) => QueryResponseModel.fromJson(json.decode(str));


class QueryResponseModel {
    int? start;
    int? queryResponseModelNumFound;
    int? numFound;
    List<Doc>? docs;

    QueryResponseModel({
         this.start,
     this.queryResponseModelNumFound,
      this.numFound,
   this.docs,
    });

    factory QueryResponseModel.fromJson(Map<String, dynamic> json) => QueryResponseModel(
        start: json["start"],
        queryResponseModelNumFound: json["num_found"],
        numFound: json["numFound"],
        docs: List<Doc>.from(json["docs"].map((x) => Doc.fromJson(x))),
    );
}

class Doc {
    String? titleSuggest;
    List<String>? editionKey;
    int? coverI;
    List<String>? isbn;
    Bloc<bool>? favorite =Bloc<bool>(init: false);
    bool? hasFulltext;
    List<String>? idDepsitoLegal;
    List<String>? text;
    List<String>? authorName;
    List<String>? contributor;
    List<String>? iaLoadedId;
    List<String>? seed;
    List<String>? oclc;
    List<String>? idGoogle;
    List<String>? ia;
    List<String>? authorKey;
    List<String>? subject;
    String? title;
    String? lendingIdentifierS;
    String? iaCollectionS;
    int? firstPublishYear;
    Type? type;
    int? ebookCountI;
    List<String>? publishPlace;
    List<String>? iaBoxId;
    int? editionCount;
    String? key;
    List<String>? idAlibrisId;
    List<String>? idGoodreads;
    List<String>? authorAlternativeName;
    bool? publicScanB;
    List<String>? idOverdrive;
    List<String>? publisher;
    List<String>? idAmazon;
    List<String>? idPaperbackSwap;
    List<String>? idCanadianNationalLibraryArchive;
    List<String>? language;
    List<String>? lccn;
    int? lastModifiedI;
    String? lendingEditionS;
    List<String>? idLibrarything;
    String? coverEditionKey;
    List<String>? person;
    List<int>? publishYear;
    String? printdisabledS;
    List<String>? place;
    List<String>? time;
    List<String>? publishDate;
    List<String>? idWikidata;
    List<String>? firstSentence;
    String? subtitle;

    Doc({
           this.titleSuggest,
         this.editionKey,
         this.coverI,
        this.isbn,
       this.hasFulltext,
    this.idDepsitoLegal,
        this.text,
        this.authorName,
        this.contributor,
        this.iaLoadedId,
        this.seed,
        this.oclc,
        this.idGoogle,
        this.ia,
        this.authorKey,
        this.subject,
        this.title,
        this.lendingIdentifierS,
        this.iaCollectionS,
        this.firstPublishYear,
        this.type,
        this.ebookCountI,
        this.publishPlace,
        this.iaBoxId,
        this.editionCount,
        this.key,
        this.idAlibrisId,
        this.idGoodreads,
        this.authorAlternativeName,
        this.publicScanB,
        this.idOverdrive,
        this.publisher,
        this.idAmazon,
        this.idPaperbackSwap,
        this.idCanadianNationalLibraryArchive,
        this.language,
        this.lccn,
        this.lastModifiedI,
        this.lendingEditionS,
        this.idLibrarything,
        this.coverEditionKey,
        this.person,
        this.publishYear,
        this.printdisabledS,
        this.place,
        this.time,
        this.publishDate,
        this.idWikidata,
        this.firstSentence,
        this.subtitle,
        this.favorite
    });

    factory Doc.fromJson(Map<String, dynamic> json) => Doc(
        favorite: Bloc<bool>(init: false),
        titleSuggest: json["title_suggest"],
        editionKey: json["edition_key"] == null ? null : List<String>.from(json["edition_key"].map((x) => x)),
        coverI: json["cover_i"] == null ? null : json["cover_i"],
        isbn: json["isbn"] == null ? null : List<String>.from(json["isbn"].map((x) => x)),
        hasFulltext: json["has_fulltext"],
        text:json["text"] == null ? null:List<String>.from(json["text"]?.map((x) => x)),
        authorName: json["author_name"] == null ? null : List<String>.from(json["author_name"].map((x) => x)),
        contributor: json["contributor"] == null ? null : List<String>.from(json["contributor"].map((x) => x)),
        iaLoadedId: json["ia_loaded_id"] == null ? null : List<String>.from(json["ia_loaded_id"].map((x) => x)),
        seed:json["seed"] == null ? null :List<String>.from(json["seed"].map((x) => x)),
        oclc: json["oclc"] == null ? null : List<String>.from(json["oclc"].map((x) => x)),
        ia: json["ia"] == null ? null : List<String>.from(json["ia"].map((x) => x)),
        authorKey: json["author_key"] == null ? null : List<String>.from(json["author_key"].map((x) => x)),
        subject: json["subject"] == null ? null : List<String>.from(json["subject"].map((x) => x)),
        title: json["title"],
        lendingIdentifierS: json["lending_identifier_s"] == null ? null : json["lending_identifier_s"],
        iaCollectionS: json["ia_collection_s"] == null ? null : json["ia_collection_s"],
        firstPublishYear: json["first_publish_year"] == null ? null : json["first_publish_year"],
        type: typeValues.map[json["type"]],
        ebookCountI: json["ebook_count_i"],
        publishPlace: json["publish_place"] == null ? null : List<String>.from(json["publish_place"].map((x) => x)),
        iaBoxId: json["ia_box_id"] == null ? null : List<String>.from(json["ia_box_id"].map((x) => x)),
        editionCount: json["edition_count"],
        key: json["key"],
        idGoodreads: json["id_goodreads"] == null ? null : List<String>.from(json["id_goodreads"].map((x) => x)),
        authorAlternativeName: json["author_alternative_name"] == null ? null : List<String>.from(json["author_alternative_name"].map((x) => x)),
        publicScanB: json["public_scan_b"] == null ? null : json["public_scan_b"],
        publisher: json["publisher"] == null ? null : List<String>.from(json["publisher"].map((x) => x)),
        language: json["language"] == null ? null : List<String>.from(json["language"].map((x) => x)),
        lccn: json["lccn"] == null ? null : List<String>.from(json["lccn"].map((x) => x)),
        lastModifiedI: json["last_modified_i"],
        lendingEditionS: json["lending_edition_s"] == null ? null : json["lending_edition_s"],
        idLibrarything: json["id_librarything"] == null ? null : List<String>.from(json["id_librarything"].map((x) => x)),
        coverEditionKey: json["cover_edition_key"] == null ? null : json["cover_edition_key"],
        publishYear: json["publish_year"] == null ? null : List<int>.from(json["publish_year"].map((x) => x)),
        printdisabledS: json["printdisabled_s"] == null ? null : json["printdisabled_s"],
        place: json["place"] == null ? null : List<String>.from(json["place"].map((x) => x)),
        publishDate: json["publish_date"] == null ? null : List<String>.from(json["publish_date"].map((x) => x)),
        firstSentence: json["first_sentence"] == null ? null : List<String>.from(json["first_sentence"].map((x) => x)),
        subtitle: json["subtitle"] == null ? null : json["subtitle"],
    );
}

enum Type { WORK }

final typeValues = EnumValues({
    "work": Type.WORK
});

class EnumValues<T> {
    Map<String, T> map;
    Map<T, String>? reverseMap;

    EnumValues(this.map);

    Map<T, String>? get reverse {
        if (reverseMap == null) {
            reverseMap = map.map((k, v) => new MapEntry(v, k));
        }
        return reverseMap;
    }
}
