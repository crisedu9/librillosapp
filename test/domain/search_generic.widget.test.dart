import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:openlibro/presentation/home/home_view_bloc.dart';
import 'package:openlibro/presentation/home/widgets/search_generic.widget.dart';

void main() {
  //String validator into search
  test('empty String returns error string', () {
    final result = StringFieldValidator.validate('');
    expect(result, 'String can\'t be empty');
  });

  testWidgets('Enter text in the text field', (WidgetTester tester) async {
    final HomeViewBloc homeBlock = HomeViewBloc();
    // Build the widget
    await tester.pumpWidget(
        MaterialApp(home: Scaffold(body: SearchGeneric(homeBlock))));
    // Enter 'Carlos' into the TextField.
    await tester.enterText(find.byKey(Key("Busqueda")), 'Carlos');
  });

}
