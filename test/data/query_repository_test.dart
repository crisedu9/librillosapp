
import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http_mock_adapter/http_mock_adapter.dart';

void main() async {
  final dio = Dio();
  final dioAdapter = DioAdapter(dio: dio);
  test('Test book queryParameters', () async {
    const books = [
      {'title': 'Don Carlos'},
      {'author_name': 'Friedrich Schiller'},
    ];
  dio.httpClientAdapter = dioAdapter;
  const path = 'http://openlibrary.org/search.json?';

  dioAdapter
    ..onGet(
      path,
          (request) => request.reply(200, books),
      queryParameters: {"q": 'Carlos',"mode":"ebooks","has_fulltext":true}
    );
  // {message: Successfully mocked GET!}
    Response  response = await dio.get(
      path,
      queryParameters: {"q": 'Carlos',"mode":"ebooks","has_fulltext":true},
    );
  expect(books, response.data);
  });
}